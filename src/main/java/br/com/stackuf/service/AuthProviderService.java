package br.com.stackuf.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufframework.domain.Usuario;
import br.ufframework.repository.UsuarioRepository;
import br.ufframework.service.AbstractAuthProviderService;

@Component
public class AuthProviderService extends AbstractAuthProviderService<Usuario> {
	
	@Autowired
	UsuarioRepository repository;
	
	@Override
	protected UsuarioRepository<Usuario> getUsuarioRepository() {
		return repository;
	}

}