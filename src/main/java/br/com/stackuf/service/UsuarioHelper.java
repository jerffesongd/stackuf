package br.com.stackuf.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import br.ufframework.domain.Usuario;
import br.ufframework.helper.AbstractUsuarioHelper;
import br.ufframework.repository.UsuarioRepository;

@ControllerAdvice
@Component
public class UsuarioHelper extends AbstractUsuarioHelper<Usuario> {
	
	private Usuario usuarioLogado;
	
	@Autowired
	UsuarioRepository<Usuario> usuarioRepository;
	
	@ModelAttribute("usuarioLogado")
	@Override
	public Usuario getUsuarioLogado() {
		if(getLoginUsuario() != null) {
			if(usuarioLogado != null && !usuarioLogado.getLogin().equals(getLoginUsuario()))
				usuarioLogado = null;
			
			if(usuarioLogado == null)
				usuarioLogado = usuarioRepository.findByLogin(getLoginUsuario());		
		}
		return usuarioLogado;
	}
	
	public String getLoginUsuario() {	
		if(SecurityContextHolder.getContext().getAuthentication() != null)
			return (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return null;
	}
	

}
