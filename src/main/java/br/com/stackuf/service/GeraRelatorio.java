package br.com.stackuf.service;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufframework.domain.Post;
import br.ufframework.repository.PostRepository;
import br.ufframework.service.AbstractGeraRelatorio;

@Service
public class GeraRelatorio implements AbstractGeraRelatorio{
	@Autowired
	PostRepository postRepository;
	
	public GeraRelatorio() {
	}
	
	@Override
	public Map<String, Integer> gerar() {
		Map<String, Integer> valores = new LinkedHashMap<String, Integer>();
		
		List<Post>  posts = postRepository.findAll();
		
		posts.sort(new PostComparator());
		
		for(Post post : posts) {
			valores.put(post.getTitulo(), post.getRespostas().size());
		}
		
		return valores;
	}

}

class PostComparator implements Comparator {
	@Override
	public int compare(Object o1, Object o2) {
		Post post1 = (Post) o1;
		Post post2 = (Post) o2;
		if(post1.getRespostas().size() == post2.getRespostas().size()) {
			return 0;
		}else if(post1.getRespostas().size() < post2.getRespostas().size()) {
			return 1;
		}else {
			return -1;
		}
	}
}
