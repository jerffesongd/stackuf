package br.com.stackuf.service;

import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufframework.domain.Grupo;
import br.ufframework.repository.GrupoRepository;
import br.ufframework.telegram.handler.MessageReceiverTemplate;

@Service
public class TelegramMessageHandler extends MessageReceiverTemplate {
	
	@Autowired
	private GrupoRepository grupoRepository;

	@Override
	public Collection<? extends Grupo> consultarGrupos(int idUsuarioExterno) {
		return Collections.emptyList();
	}

	@Override
	public String getGrupoCommand() {
		return "/grupo";
	}

	@Override
	public String getDescricaoGrupoCommand() {
		return "Seleciona um dos seguintes grupos";
	}
	
}
