package br.com.stackuf.domain;

import br.ufframework.domain.Permissao;

public class PermissoStack extends Permissao {
	
	public static final String ADM = "ADM";
	public static final String MODERADOR = "MODERADOR";
	public static final String USUARIO = "USUARIO";
	
}
