package br.com.stackuf.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import br.com.stackuf.domain.PermissoStack;
import br.com.stackuf.service.AuthProviderService;
import br.ufframework.config.WebSecurityConfigurationAbstract;
import br.ufframework.service.AbstractAuthProviderService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurationAbstract {
	
	@Autowired
	AuthProviderService auth;
	
	PermissoStack permissoes;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		/**
		 * permissao das paginas.
		 */
		// TODO Auto-generated method stub
		http
	        .authorizeRequests()
	        .antMatchers("/css/**").permitAll()
	        .antMatchers("/img/**").permitAll()
	        .antMatchers("/fontawesome/**").permitAll()
	        .antMatchers("/js/**").permitAll()
        	.antMatchers("/post/visualizar/**").permitAll()
        	.antMatchers("/post/**").hasAnyAuthority(permissoes.ADM, permissoes.MODERADOR)
        	.antMatchers("/relatorio/**").hasAnyAuthority(permissoes.ADM)
        	.antMatchers("/templates/**").hasAnyAuthority(permissoes.ADM)
			.antMatchers("/grupos/**").hasAnyAuthority(permissoes.ADM);
			
		http.csrf().disable();
		super.configure(http);
	}
		
	@Override
	protected String pageLoginSucess() {
		// URL para qual o uruário será redirecionado quando logar com sucesso
		return "/";
	}

	@Override
	protected String pageLogoutSucess() {
		// TODO Auto-generated method stub
		return "/";
	}

	@Override
	protected AbstractAuthProviderService<?> getAuthProvider() {
		// TODO Auto-generated method stub
		return auth;
	}

}
