package br.com.stackuf.controller;

import org.springframework.web.bind.annotation.RestController;

import br.com.stackuf.service.TelegramMessageHandler;
import br.ufframework.telegram.controller.TelegramController;

/**
 * 
 * @author Lucas Alessio
 *
 */
@RestController
public class TelegramControllerImpl extends TelegramController<TelegramMessageHandler> {

}
