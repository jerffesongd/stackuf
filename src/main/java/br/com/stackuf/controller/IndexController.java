package br.com.stackuf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import br.ufframework.controller.AbstractIndexController;
import br.ufframework.helper.AbstractUsuarioHelper;
import br.ufframework.repository.GrupoRepository;
import br.ufframework.repository.PostRepository;

@Controller
public class IndexController extends AbstractIndexController{
	
	@Autowired
	protected PostRepository postRepository;
	
	@Autowired
	private GrupoRepository grupoRepository;
	
	@Override
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView("/index.html");
		
		modelAndView.addObject("posts", postRepository.findAllByDataCriacao());
		modelAndView.addObject("grupos", grupoRepository.findAll());
		
		return modelAndView;
	}
	
	@GetMapping("/grupos")
	public ModelAndView turmas() {
		ModelAndView modelAndView = new ModelAndView("index.html");
		
		return modelAndView;
	}

}
