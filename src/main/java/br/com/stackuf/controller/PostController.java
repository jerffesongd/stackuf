package br.com.stackuf.controller;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import br.com.stackuf.service.NotificacaoService;
import br.ufframework.controller.AbstractPostController;
import br.ufframework.domain.ForumOperacoes;
import br.ufframework.domain.Post;
import br.ufframework.service.PostService;


@Controller
@RequestMapping("/post")
public class PostController extends AbstractPostController{
	
	protected Object getGrupo(Integer idTurma) {
		return null;
	}
	@Autowired
	PostService postService;
	
	@Autowired
	private NotificacaoService notificacaoService;
	
	@Override
	public ModelAndView salvar(Post post, BindingResult result, RedirectAttributes ra) {
		
		ModelAndView modelAndView = new ModelAndView(new RedirectView("/", true));
		
		try {
			post.setStatus("CADASTRADO");
			post.setResponsavel(usuarioHelper.getUsuarioLogado());
			postService.validar(post);
			postRepository.save(post);
			notificacaoService.notificar(ForumOperacoes.CADASTRAR_POST, post, null, Arrays.asList(post.getResponsavel()));
			modelAndView = new ModelAndView(new RedirectView("/post/visualizar/"+post.getId(), true));
		} catch (NullPointerException np) {
			mensagemHelper.erro(ra, "msg.valores.vazios");
		} catch (Exception e) {
			mensagemHelper.erro(ra, "msg.erro");
		}
		
		
		return modelAndView;
	}
	
	@Override
	public ModelAndView novoPost(@PathVariable Integer idGrupo) {
				
		ModelAndView modelAndView = new ModelAndView("post_form");
		modelAndView.addObject("grupos", grupoRepository.findAll());
		modelAndView.addObject("post", new Post());
		
		return modelAndView;
	}
	
	
	@Override
	public ModelAndView editarPost(@PathVariable Integer idPost) {
		ModelAndView modelAndView = new ModelAndView("post_form");
		Post post = postRepository.buscarPorId(idPost);
		this.idGrupo = post.getGrupo().getId();
		modelAndView.addObject("post", post);
		modelAndView.addObject("grupos", grupoRepository.findAll());
		return modelAndView;
	}	


}

