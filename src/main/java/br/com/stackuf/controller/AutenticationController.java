package br.com.stackuf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import br.ufframework.controller.AbstractAutenticationController;
import br.ufframework.domain.Permissao;
import br.ufframework.domain.Usuario;
import br.ufframework.repository.PermissaoRepository;
import br.ufframework.repository.UsuarioRepository;
/**
 * 
 * @author Lucas Alessio
 *
 */
@Controller
public class AutenticationController extends AbstractAutenticationController {
	
	@Autowired
	UsuarioRepository<Usuario> usuarioRepository;
	
	@Autowired
	PermissaoRepository permissaoRepository;
	
	@Override
	@GetMapping("/autenticacao")
	public String formAutenticacao(Model model) {
		
		preencherBanco(); // Cria um usuário caso não exista exista nenhum
		
		return "login.html";
	}
	

	private void preencherBanco() {
		
		List<Usuario> u =usuarioRepository.findAll();
		List<Permissao> p = permissaoRepository.findAll();
		
		if(p.isEmpty()) {
			
			Permissao adm = new Permissao();
			Permissao moderador = new Permissao();
			Permissao usuario = new Permissao();
			
			adm.setCodigo("ADM");
			adm.setDescricao("Administrador");
			
			moderador.setCodigo("MODERADOR");
			moderador.setDescricao("Moderador");
			
			usuario.setCodigo("USUARIO");
			usuario.setDescricao("usuario");
			
			permissaoRepository.save(adm);
			permissaoRepository.save(moderador);
			permissaoRepository.save(usuario);
			
		}
		
		if(u.isEmpty()) {
			
			Usuario usuarioADM = new Usuario();
			Usuario usuarioGESTOR = new Usuario();
			Usuario usuarioComum = new Usuario();
			
			
			usuarioADM.setLogin("admin");
			usuarioADM.setEmail("jerffeson.gomes@imd.ufrn.br");
			usuarioADM.setNomePessoa("ADM Gomes");
			usuarioADM.setSenha("admin");
			
			usuarioGESTOR.setLogin("gestor");
			usuarioGESTOR.setEmail("jerffeson.gomes@imd.ufrn.br");
			usuarioGESTOR.setNomePessoa("Gestor Dutra");
			usuarioGESTOR.setSenha("gestor");
			
			usuarioComum.setLogin("usuario");
			usuarioComum.setEmail("jerffeson.gomes@imd.ufrn.br");
			usuarioComum.setNomePessoa("Usuario Comum");
			usuarioComum.setSenha("usuario");
			
			usuarioRepository.save(usuarioADM);
			usuarioRepository.save(usuarioGESTOR);
			usuarioRepository.save(usuarioComum);
			
		}
		
		
		
	}



	@GetMapping("/logout")
	public ModelAndView logout() {
		ModelAndView modelAndView = new ModelAndView(new RedirectView("https://autenticacao.info.ufrn.br/sso-server/logout?service=http://localhost:8080/uforum/"));
		return modelAndView;
	}

}
