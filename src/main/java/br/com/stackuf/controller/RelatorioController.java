package br.com.stackuf.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.stackuf.service.GeraRelatorio;
import br.ufframework.controller.AbstractRelatorioController;

@Controller
@RequestMapping("/relatorio")
public class RelatorioController extends AbstractRelatorioController{
	@Autowired
	GeraRelatorio geraRelatorio;
	//Adiciona Uma classe com o algoritmo para gerar relatorio
	public RelatorioController() {
	}
	
	@Override
	public ModelAndView mostrarRelatorio() {
		super.setGeraRelatorio(geraRelatorio);
		return super.mostrarRelatorio();
	}

}

