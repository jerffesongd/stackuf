package br.com.stackuf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages="${scan.packages}")
@EnableJpaRepositories({"br.ufframework.repository", "br.ufframework.telegram.repository"})
@EntityScan({"br.ufframework.domain", "br.ufframework.telegram.dto"})
public class StackufApplication {

	public static void main(String[] args) {
		SpringApplication.run(StackufApplication.class, args);
	}
}
